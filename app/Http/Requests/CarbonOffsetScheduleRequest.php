<?php

namespace App\Http\Requests;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class CarbonOffsetScheduleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $today = Carbon::now()->isoFormat('YYYY-MM-DD');

        return [
            'subscriptionStartDate' => ['required','date_format:Y-m-d',"before_or_equal:$today"],
            'scheduleInMonths' => ['required','integer','min:0','max:36']
        ];
    }
}
