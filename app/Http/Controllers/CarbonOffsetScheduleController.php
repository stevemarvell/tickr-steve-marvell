<?php

namespace App\Http\Controllers;

use App\Http\Requests\CarbonOffsetScheduleRequest;
use Carbon\CarbonImmutable;
use \Illuminate\Http\JsonResponse;

class CarbonOffsetScheduleController extends Controller
{
    function index(CarbonOffsetScheduleRequest $request): JsonResponse
    {
        // validated because of the rules in the request

        $subscriptionStartDate = $request->get('subscriptionStartDate');
        $scheduleInMonths = $request->get('scheduleInMonths');

        if ($scheduleInMonths == 0) {
            return response()->json([]);
        }

        $start = CarbonImmutable::create($subscriptionStartDate);

        $dates = array_map(
            function($i) use ($start) { return $start->addMonthsNoOverflow($i)->isoFormat('YYYY-MM-DD'); },
            range(1,$scheduleInMonths));

        return response()->json($dates);
    }
}
