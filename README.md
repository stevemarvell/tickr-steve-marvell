# Tickr Tech Test 

A tech test for Tickr.

No details of the test will be presented here.

*WARNING* This project is not for production and should be run in a development environment only.

A `.env` file has been supplied for convenience only. 

## Installation

Clone the repository:

Retrieve source using an option through the _Clone_ button or directly using:

`git clone https://bitbucket.org/stevemarvell/tickr-steve-marvell.git`

`cd tickr-steve-marvell`

`composer install`

If you don't have `composer` then head to https://getcomposer.org/

Depending on your platform, you may need to run the `composer.phar`. See the site for info.

## Running

`php artisan serve`

You will be told where you can access the server.

If you experiment using a web browser then you will be able to generate error messages.

## Testing

This project uses `behat` for testing, so all the tests are in the `features` directory.

Having installed...

Run up a different host.

`php artisan serve --env=behat`

This will be a different URL, unless changed in `.env.behat` file.

In another terminal run:

`vendor/bin/behat`

## Notes

There is no point creating a provider or repository until the wider business logic is determined. 
Leave it in the controller for now. 

Also create a different `behat` context when the scope is known.

Date specification is not ISO8601 as it's only the date part

## TODO

* Clean up such as env files
* Name the application
* Remove all the excess User modeling 
* Make the date format error message nicer Y-d-m => YYYY-MM-DD without destroying the attribute relationship in the rule
* Create test for method (405) and resource (404) problems and possibly some 403
* Handle the above more gracefully for API
* Use a faker for testing dates
* Split the final test into a lot of tests
* Put some messages in the tests
* Test the error strings
* Configure `behat` to use a testing env in combination
