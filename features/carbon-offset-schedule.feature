Feature: Carbon Offset Schedule
    In order to get a carbon offset schedule
    A subscriptionStartDate and a scheduleInMonths must be specified
    The schedule is number of dates
    from the subscriptionStartDate
    at one month intervals
    for the next scheduleInMonths months after the subscriptionStartDate

    Rules:
    - endpoint method is GET only
    - endpoint resource is /carbon-offset-schedule
    - the parameters are supplied as query string
    - endpoint returns json
    - the content us a list of dates as strings
    - the dates are in YYYY-MM-DD format
    - the dates are valid
    - the dates are sorted in ascending order
    - if the subscriptionStartDate day is higher than "this month" then an appropriate "end of month" day is chosen
    - response status code 400 on missing parameters
    - response status code 400 on invalid parameters
    - errors should be returned
    - subscriptionStartDate format is of form YYYY-MM-DD
    - subscriptionStartDate should be not be in the future
    - scheduleInMonths format is integer
    - scheduleInMonths has the range 0 <= value <= 36

    Scenario Outline: missing parameters
        When query parameters are set to <subscriptionStartDate> <scheduleInMonths>
        And request is submitted
        Then response status code is <statusCode>
        Examples:
            | subscriptionStartDate | scheduleInMonths | statusCode |
            | :EMPTY:               | :EMPTY:          | 400        |
            | 2000-01-01            | :EMPTY:          | 400        |
            | :EMPTY:               | 1                | 400        |

    Scenario Outline: check subscriptionStartDate
        When query parameters are set to <subscriptionStartDate> <scheduleInMonths>
        And request is submitted
        Then response status code is <statusCode>
        Examples:
            | subscriptionStartDate | scheduleInMonths | statusCode |
            |                       | 1                | 400        |
            | someString            | 1                | 400        |
            | 01/01/2000            | 1                | 400        |
            | 2000-01-00            | 1                | 400        |
            | 2000-01-01            | 1                | 200        |
            | :yesterday:           | 1                | 200        |
            | :today:               | 1                | 200        |
            | :tomorrow:            | 1                | 400        |

    Scenario Outline: check subscriptionMonths
        When query parameters are set to <subscriptionStartDate> <scheduleInMonths>
        And request is submitted
        Then response status code is <statusCode>
        Examples:
            | subscriptionStartDate | scheduleInMonths | statusCode |
            | 2000-01-01            | -1               | 400        |
            | 2000-01-01            | 0                | 200        |
            | 2000-01-01            | 1                | 200        |
            | 2000-01-01            | 1.0              | 400        |
            | 2000-01-01            | 1.5              | 400        |
            | 2000-01-01            | 2                | 200        |
            | 2000-01-01            | 35               | 200        |
            | 2000-01-01            | 36               | 200        |
            | 2000-01-01            | 37               | 400        |

    Scenario Outline: tickr passes
        When query parameters are set to <subscriptionStartDate> <scheduleInMonths>
        And request is submitted
        Then response json body is <responseBody>
        Examples:
            | subscriptionStartDate | scheduleInMonths | responseBody |
            | 2021-01-07            | 5                | ["2021-02-07", "2021-03-07", "2021-04-07", "2021-05-07", "2021-06-07"] |
            | 1998-01-07            | 2                | ["1998-02-07", "1998-03-07"]                                           |
            | 2020-01-30            | 3                | ["2020-02-29", "2020-03-30", "2020-04-30"]                             |
            | 2020-01-31            | 1                | ["2020-02-29"]                                                         |
            | 2021-01-10            | 0                | []                                                                     |

    Scenario Outline: tickr failures
        When query parameters are set to <subscriptionStartDate> <scheduleInMonths>
        And request is submitted
        Then response status code is <statusCode>
        Examples:
            | subscriptionStartDate | scheduleInMonths | statusCode |
            | 2031-01-30            | 2                | 400        |
            | 2020-01-30            | 200              | 400        |
