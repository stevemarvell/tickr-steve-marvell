<?php

use Behat\Behat\Context\Context;
use Behat\Behat\Tester\Exception\PendingException;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Carbon\Carbon;
use Tests\TestCase;


/**
 * Defines application features from the specific context.
 */
class ApiContext extends TestCase implements Context
{
    private static $requestResource = "carbon-offset-schedule"; // not slash or conflicts with base

    private $query;
    private $response;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
        parent::__construct();

        // this is the important bit

        parent::setUp();
    }

    /**
     * @When /^query parameters are set to (.*) (.*)$/
     * @param $subscriptionStartDate
     * @param $scheduleInMonths
     */
    public function queryParametersAreSetTo($subscriptionStartDate, $scheduleInMonths)
    {
        $dateString = $subscriptionStartDate;

        if (preg_match('(:(?:yesterday|today|tomorrow):)', $subscriptionStartDate,$matches)) {

            $placeholder = $matches[0];

            $date = "";

            switch ($placeholder) {
                case ":yesterday:":
                    $date = Carbon::now()->subDay();
                    break;
                case ":today:":
                    $date = Carbon::now();
                    break;
                case ":tomorrow:":
                    $date = Carbon::now()->addDay();
                    break;
            }

            $dateString = $date->toDateString();
        }

        $this->query = [];

        if ($dateString != ':EMPTY:') {
            $this->query['subscriptionStartDate'] = $dateString;
        }

        if ($scheduleInMonths != ':EMPTY:') {
            $this->query['scheduleInMonths'] = $scheduleInMonths;
        }
    }

    /**
     * @Given /^request is submitted$/
     */
    public function requestIsSubmitted()
    {
        $this->response = $this->call('GET', static::$requestResource, $this->query);
    }

    /**
     * @Then /^response status code is (.*)$/
     * @param $statusCode
     */
    public function responseStatusCodeIs(int $statusCode)
    {
        $this->response->assertStatus($statusCode);
    }

    /**
     * @Then /^response json body is (.*)$/
     * @param $responseBody
     */
    public function responseJsonBodyIs($responseBody)
    {
        $this->response->assertJson(json_decode($responseBody));
    }
}
